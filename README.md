# MCNP Tally Parser

This is a very quick (read: simple) parser for MCNP binned tally output that I wrote up because PyNE was not installing and I could not be asked to troubleshoot.

## Example Usage
```python
output_text = read_output('output/output_gammas')

bins, metric, uncertainty = parse_tally(text, n_bins=51, bin_type='time')

plot_binned_tally(bins, metric, uncertainty, title='Gamma tally, time binned', xlabel='time bin, shakes', ylabel='weighted current, particles')
```
